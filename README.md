# SEP-All-Clear-API

* [INFO](#markdown-header-info)
* [TEST BUILD](#markdown-header-test-build)
* [CONFIGURATION](#markdown-header-configuration)

## TEST Build

The build section is only for building and running on an individual basis for testing. If deploying the entire system,
go to the main deployment process utilizing `docker-compose`, see the
main [deployment project](https://bitbucket.org/gsudmlab/isepcleardeploy).

If you wish to continue to run just the local project, continue to [TEST_BUILD.md](TEST_BUILD.md)

***

# CONFIGURATION

The default config file for this process is config.ini. It is copied into the image at build time.

A full listing of the lines lines in the config file and what they control are listed in the [CONFIG.md](CONFIG.md)
file.

***
***

## Acknowledgment

This work was supported in part by NASA Grant Award No. NNH14ZDA001N, NASA/SRAG Direct Contract and two NSF Grant
Awards: No. AC1443061 and AC1931555.

***

This software is distributed using the [GNU General Public License, Version 3](./LICENSE.txt) 

![alt text](./images/gplv3-88x31.png)

***

© 2022 Dustin Kempton, Berkay Aydin, Rafal Angryk

[Data Mining Lab](http://dmlab.cs.gsu.edu/)

[Georgia State University](http://www.gsu.edu/)
