# Listing of the SEP-All-Cear-API config file lines

## TOC

* [Return to README](./README.md)
* [DATABASE](#markdown-header-database)
* [LOGGING](#markdown-header-logging)
* [RUNTIME](#markdown-header-runtime)

***

## DATABASE

This set of values is for configuration of the database connection. We must have each of the sub values within the  __
DATABASE__  tag. If they are not present, the configuration will fail. They do not need to be in any particular order
though.

	[DATABASE]
	host = mysql-box
	database = test
	user = root
	password = root123
	pool_size = 3
	port = 3306

### host

	host = mysql-box

This is the hostname of the container or server that holds the MySQL relational database managment system that is used
to hold all of the data produced by and accessed by this process.

### database

	database = test

This is the schema name of the database within the MySQL relational database managment system that is used to hold all
of the data produced by and accessed by this process.

### user

	user = root

This is the username to use when connecting to the MySQL relational database managment system that is used to hold all
of the data produced by and accessed by this process.

### password

	password = root123

This is the password to use when connecting to the MySQL relational database managment system that is used to hold all
of the data produced by and accessed by this process.

### pool_size

	pool_size = 3

The process constructs a pool of database connections, this is the size of that pool of connections to the MySQL
relational database managment system that is used to hold all of the data produced by and accessed by this process.

### port

	port = 3306

This is the port that is exposed on the host that contains the MySQL relational database managment system that is used
to hold all of the data produced by and accessed by this process.

[Return to TOC](#markdown-header-toc)

***

## LOGGING

This set of values is for configuration of logging of the process. We must have each of the sub values within the  __
LOGGING__  tag. If they are not present, the configuration will fail. They do not need to be in any particular order
though.

	[LOGGING]
	log_path = /app/log
	log_file = sep-all-clear-api.log
	log_file_size_bytes = 1048576
	log_backups = 5
	level = DEBUG

### log_path

	log_path = /app/log

This is the location within the container that the process will write the log file. This is provided so it can be set to
a location that is mapped outside the container to a location of the users choosing. Doing such mapping makes the log
file avialable outside of the container.

### log_file

	log_file = sep-all-clear-api.log

This provides the naming convention for the set of rolling log files. The most current log file will be named as the
provided file name, and the rollover files will be numbered 1 to the number of backup files specified.

### log_file_size_bytes

	log_file_size_bytes = 1048576

This provides how large the log file will be allowed to grow in bytes prior to it being rolledover to a backup file and
a new empty file is started for fresh logging.

### log_backups

	log_backups = 5

This provides how many backup log files to keep after rollover has taken place. Each time a new file is created, the old
files are rolled over up to this number of times before they are simply deleted.

### level

	level = DEBUG

This provides the level of logging to perform. The levels available are  __DEBUG__ ,  __INFO__ , or  __ERROR__ . 
**Note**: These are case sensitive! In the  __DEBUG__  mode, the most amount of information is provided, including stack
traces of errors that occur. In the  __INFO__  mode, slightly less information is recoreded, like times of start and
completion of the scheduled processing task but stack traces of errors are not recorded. In the  __ERROR__  mode, the
least amount of information is recorded. In this, only the error information is recorded, this includes what method the
error was captured in and what information the error provided, but no stack trace.

[Return to TOC](#markdown-header-toc)

***

## RUNTIME

This set of values is for configuration of the runtime frequency of the various sub-processes within this process. We
must have each of the sub values within the  __RUNTIME__  tag. If they are not present, the configuration will fail.
They do not need to be in any particular order though.

	[RUNTIME]
    debug = True

### debug

	debug = True

This tells the web application to provide debug information when an incorrect URL is entered. This should be set to
false at runtime.

[Return to TOC](#markdown-header-toc)

***

[Return to README](./README.md)

***
***

## Acknowledgment

This work was supported in part by NASA Grant Award No. NNH14ZDA001N, NASA/SRAG Direct Contract and two NSF Grant
Awards: No. AC1443061 and AC1931555.

***

This software is distributed using the [GNU General Public License, Version 3](./LICENSE.txt)

![alt text](./images/gplv3-88x31.png)

***

© 2022 Dustin Kempton, Berkay Aydin, Rafal Angryk

[Data Mining Lab](http://dmlab.cs.gsu.edu/)

[Georgia State University](http://www.gsu.edu/)