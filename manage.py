"""
 * SEP-All-Clear-API, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import os
import sys
import py_src.configuration.ConfigReader as conf
from py_src.configuration import settings as settings


def setup(config_dir, config_file):
    config_dir = os.path.abspath(config_dir)
    config = conf.ConfigReader(config_dir, config_file)

    settings.initialize_db(config.get_pool_info())
    settings.set_debug(config.get_is_debug())
    settings.initialize_logging(config.get_logger_info())

def main():
    setup(os.path.join(os.getcwd(), 'config'), 'config.ini')

    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'py_src.configuration.settings')

    from django.core.management import execute_from_command_line
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
