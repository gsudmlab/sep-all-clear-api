"""
WSGI config for SEP_ALL_CLEAR_API project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os
import sys
import py_src.configuration.ConfigReader as conf
from py_src.configuration import settings as settings

from django.core.wsgi import get_wsgi_application

# this is a pointer to the module object instance itself.
this = sys.modules[__name__]

# we can explicitly make assignments on it
this.is_setup = False


def setup(config_dir, config_file):
    if settings.DATABASES is None:
        config_dir = os.path.abspath(config_dir)
        config = conf.ConfigReader(config_dir, config_file)
        settings.initialize_db(config.get_pool_info())
        settings.set_debug(config.get_is_debug())
        settings.initialize_logging(config.get_logger_info())
    this.is_setup = True


if not this.is_setup:
    setup(os.path.join(os.getcwd(), 'config'), 'config.ini')

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'py_src.configuration.settings')

application = get_wsgi_application()
