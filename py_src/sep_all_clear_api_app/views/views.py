import pytz
import datetime
from py_src.sep_all_clear_api_app.models.models import AggregateResults
from django.forms.models import model_to_dict
from django.http.response import JsonResponse


# Method returns latest value if no query provided or if blank url,
# Else, query is executed and nearest result is output in JSON.

def mainview(request):
	# Retrieve the query variable
	datestring3 = request.GET.get('datetime')
	if not datestring3:
		# Get all objects
		first = AggregateResults.objects.order_by('-issue_time')[0]

		# Determine Latest
		# first = allobjects_empty.order_by('-issue_time').first()

		# To Serialize into JSON:
		data_empty = model_to_dict(first)
		data_empty.pop('id')
		return JsonResponse(data_empty, safe=False)

	else:
		# Converts user input string to datetime, timedelta.
		datestring2 = datetime.datetime.strptime(datestring3, '%Y-%m-%dT%H:%M:%S')
		datestring = datestring2.replace(tzinfo=pytz.UTC)

		target = datestring

		# Blank datestring
		zerodate = datetime.datetime.strptime("2000-01-01T00:00:00", '%Y-%m-%dT%H:%M:%S')

		# Extract list of timestamps from DB:
		m = AggregateResults.objects.all()
		m2 = m.values('issue_time')
		m3 = m2.values_list('issue_time', flat=True)
		listed = list(m3)
		length = len(listed)

		# Lowest time delta
		lowesttimedelta = 999999999

		# Logic to figure out which is the closest time to input
		for i in range(length):

			currenttimedelta = abs((listed[i] - target).total_seconds())
			if currenttimedelta < lowesttimedelta:
				zerodate = listed[i]
				lowesttimedelta = currenttimedelta

		# Retrieve details of that time:
		result = zerodate
		entry = m.filter(issue_time=result)

		# JSON Serialized Output
		data = list(entry.values())
		data = data[0]
		data.pop('id')
		return JsonResponse(data, safe=False)


def scoreboard(request):
	# Retrieve the query variable
	scoreboard_report = {}
	datestring3 = request.GET.get('datetime')
	if not datestring3:
		# Get all objects
		first = AggregateResults.objects.order_by('-issue_time')[0]

		# Determine Latest
		# first = allobjects_empty.order_by('-issue_time').first()

		# To Serialize into JSON:
		data = model_to_dict(first)

		submission = {}
		scoreboard_report["sep_forecast_submission"] = submission
		model = {}
		model["short_name"] = "GSU All Clear"
		model["spase_id"] = "N/A"
		submission["model"] = model
		issue_time = data["issue_time"]
		submission["issue_time"] = issue_time
		submission["mode"] = "forecast"


	else:
		# Converts user input string to datetime, timedelta.
		datestring2 = datetime.datetime.strptime(datestring3, '%Y-%m-%dT%H:%M:%S')
		datestring = datestring2.replace(tzinfo=pytz.UTC)

		target = datestring

		# Blank datestring
		zerodate = datetime.datetime.strptime("2000-01-01T00:00:00", '%Y-%m-%dT%H:%M:%S')

		# Extract list of timestamps from DB:
		m = AggregateResults.objects.all()
		m2 = m.values('issue_time')
		m3 = m2.values_list('issue_time', flat=True)
		listed = list(m3)
		length = len(listed)

		# Lowest time delta
		lowesttimedelta = 999999999

		# Logic to figure out which is the closest time to input
		for i in range(length):

			currenttimedelta = abs((listed[i] - target).total_seconds())
			if currenttimedelta < lowesttimedelta:
				zerodate = listed[i]
				lowesttimedelta = currenttimedelta

		# Retrieve details of that time:
		result = zerodate
		entry = m.filter(issue_time=result)

		# JSON Serialized Output
		data = list(entry.values())
		data = data[0]

		submission = {}
		scoreboard_report["sep_forecast_submission"] = submission
		model = {}
		model["short_name"] = "GSU All Clear"
		model["spase_id"] = "N/A"
		submission["model"] = model
		issue_time = data["issue_time"]
		submission["issue_time"] = issue_time
		submission["mode"] = "historical"

	forecasts = []
	forecast = {}
	submission["forecasts"] = forecasts
	forecast["energy_channel"] = {"min": 10, "max": -1, "units": "MeV"}
	forecast["species"] = "proton"
	forecast["location"] = "earth"

	prediction_window = {}
	prediction_window['start_time'] = issue_time
	prediction_window['end_time'] = issue_time + datetime.timedelta(hours=12)
	forecast["prediction_window"] = prediction_window

	all_clear_prob = data['all_clear_prob']
	probabilities = []
	probability = {}
	probability['probability_value'] = 1.0 - all_clear_prob
	probability['threshold'] = 10.0
	probability['threshold_units'] = 'pfu'
	probabilities.append(probability)
	forecast['probabilities'] = probabilities

	all_clear = {}
	all_clear_threshold = 0.75
	all_clear['all_clear_boolean'] = False if all_clear_prob < all_clear_threshold else True
	all_clear['threshold'] = 10.0
	all_clear['threshold_units'] = "pfu"
	all_clear['probability_threshold'] = 1.0 - all_clear_threshold
	forecast['all_clear'] = all_clear
	forecasts.append(forecast)

	return JsonResponse(scoreboard_report, safe=False)
