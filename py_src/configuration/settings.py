import os
import sys
from pathlib import Path

# this is a pointer to the module object instance itself.
this = sys.modules[__name__]

"""
We have three instance variables that we wish to be set by another object that calls on this object
1. DATABASES: This holds the database connection information read from config.ini
2. DEBUG: The debug setting that is read from the config.ini
3. LOGGING: The logging information read from the config.ini
"""

this.DATABASES = None
this.DEBUG = None
this.LOGGING = None

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent.parent


def initialize_db(db_settings):
    if this.DATABASES is None:
        # also in local function scope. no scope specifier like global is needed
        this.DATABASES = db_settings
    else:
        msg = "Database is already initialized to {0}."
        raise RuntimeError(msg.format(this.DATABASES))


def initialize_logging(logging_settings):
    if this.LOGGING is None:
        # also in local function scope. no scope specifier like global is needed
        this.LOGGING = logging_settings
    else:
        msg = "Logging is already initialized to {0}."
        raise RuntimeError(msg.format(this.LOGGING))


def set_debug(debug_settings):
    if this.DEBUG is None:
        this.DEBUG = debug_settings
    else:
        msg = "Debug setting is already initialized to {0}."
        raise RuntimeError(msg.format(this.DEBUG))


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'op=*s&h=k27ey_%#*4)&yx*tc*ih+v424euh15xefv==&r4c#b'

ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = [
    'django.contrib.staticfiles',
    'py_src.sep_all_clear_api_app',
    'django.contrib.contenttypes',
    'rest_framework',
    'corsheaders',
]

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

MIDDLEWARE = [
    'django.middleware.common.CommonMiddleware',
    'corsheaders.middleware.CorsMiddleware',
]

ROOT_URLCONF = 'py_src.sep_all_clear_api_app.urls.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'WebContent']
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
            ],
        },
    },
]

# URL prefix for static files.
STATIC_URL = "staticfiles/"
# Determine if in Production or Development and setup different based on that
if (len(sys.argv) >=2 and sys.argv[1] == 'runserver'):
    # In development
    # Additional locations of static files
    this.STATICFILES_DIRS = [
        # location of your application, should not be public web accessible
         os.path.join(BASE_DIR, 'WebContent', 'staticfiles'),
         os.path.join(BASE_DIR, 'WebContent', 'staticfiles', 'assets'),
    ]

else:
    # In production
    # Additional locations of static files
    this.STATICFILES_DIRS = [
    ]

    # web accessible folder
    this.STATIC_ROOT = os.path.join(BASE_DIR, 'WebContent', 'staticfiles')


USE_X_FORWARDED_HOST = True

WSGI_APPLICATION = 'py_src.sep_all_clear_api_app.wsgi.application'
ASGI_APPLICATION = 'py_src.sep_all_clear_api_app.asgi.application'

CORS_ORIGIN_ALLOW_ALL = True
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True
