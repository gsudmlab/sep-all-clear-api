"""
 * SEP-All-Clear-API, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import os
import configparser


class ConfigReader:

    def __init__(self, conf_file_path: str, conf_file_name: str):
        config = configparser.ConfigParser()
        conf_file = conf_file_path + os.path.sep + conf_file_name
        config.read(conf_file)

        self._pool_dict = self.__create_pool_dict(config)
        self._logger_dict = self.__create_logger_dict(config)
        self._is_debug = bool(config['RUNTIME']['debug'] == 'True')

    @staticmethod
    def __create_pool_dict(config):

        db_setting = {
            'default': {
                'ENGINE': 'django.db.backends.mysql',
                'NAME': config['DATABASE']['database'],
                'USER': config['DATABASE']['user'],
                'PASSWORD': config['DATABASE']['password'],
                'HOST': config['DATABASE']['host'],
                'PORT': config['DATABASE']['port'],
            }
        }
        return db_setting

    @staticmethod
    def __create_logger_dict(config):
        log_level = config['LOGGING']['level']
        if log_level == "DEBUG":
            log_level = 'DEBUG'
        elif log_level == "INFO":
            log_level = 'INFO'
        else:
            log_level = 'ERROR'

        log_dir = config['LOGGING']['log_path']
        log_file = config['LOGGING']['log_file']
        log_file = os.path.realpath(os.path.join(log_dir, log_file))

        log_file_size = int(config['LOGGING']['log_file_size_bytes'])
        log_file_backups = int(config['LOGGING']['log_backups'])

        logging_dict = {
            'version': 1,
            'disable_existing_loggers': True,
            'formatters': {
                'default': {
                    'format': '%(name)s - %(asctime)s - %(levelname)s - %(message)s',
                    'style': '%',
                },
            },
            'handlers': {
                'file': {
                    'level': log_level,
                    'class': 'logging.handlers.RotatingFileHandler',
                    'filename': log_file,
                    'formatter': 'default',
                    'maxBytes': log_file_size,
                    'backupCount': log_file_backups,
                },
            },
            'loggers': {
                'django': {
                    'handlers': ['file'],
                    'propagate': False,
                    'level': log_level,
                }
            },
        }

        return logging_dict

    def get_pool_info(self):
        return self._pool_dict

    def get_logger_info(self):
        return self._logger_dict

    def get_is_debug(self):
        return self._is_debug
