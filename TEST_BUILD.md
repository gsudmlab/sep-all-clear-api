# SEP-All-Clear-API Test Build

If you wish to run this project independently on your local host for test purposes, you will need to setup several
things first.

1. The required libraries listed in the `requirements.txt` file, you should setup a conda environment with the required
   versions of the libraries by first:

       conda create -n sepapi

   Then using the `sepapi` environment by

       conda activate sepapi

   You can then install the needed libraries using:

       conda install python==3.7*


       conda install -c conda-forge Django==3.2.12 django-cors-headers==3.9.0 djangorestframework==3.12.4 mysql-connector-python mysqlclient


2. This project is dependent upon a database to be available for it to access tables from. It does not construct any
   tables and is dependent upon the tables being constructed by
   the [NRT-SEP-All-Clear-Aggregator](https://bitbucket.org/gsudmlab/nrt-sep-all-clear-aggregator) project.   **Note:**
   The current [config file](config/config.ini) setup in this project is dependent upon the database being started with
   the following command:

       docker run --name mysql-box -e MYSQL_ROOT_PASSWORD=root123 -e MYSQL_DATABASE=test -p 3306:3306 -d mysql:latest --default-authentication-plugin=mysql_native_password

   If you are already using port 3306 you need to change the first 3306, this is the port on the host that you are
   mapping port 3306 of the container to. This will be the port you can point your MySQL admin tool to in order to look
   at the tables/data from your desktop. You will also need to update the config file with the changes you have made.

3. You should now be able to run the project using:

       python3 manage.py runserver 0.0.0.0:8000

   You can then access it by using [https://localhost:8000/](https://localhost:8000/)

4. When done you should deactivate the environment:

       conda deactivate

***

[Return to README](./README.md)

***
***

## Acknowledgment

This work was supported in part by NASA Grant Award No. NNH14ZDA001N, NASA/SRAG Direct Contract and two NSF Grant
Awards: No. AC1443061 and AC1931555.

***

This software is distributed using the [GNU General Public License, Version 3](./LICENSE.txt)

![alt text](./images/gplv3-88x31.png)

***

© 2022 Dustin Kempton, Berkay Aydin, Rafal Angryk

[Data Mining Lab](http://dmlab.cs.gsu.edu/)

[Georgia State University](http://www.gsu.edu/)